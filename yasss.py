#/bin/python

# Yet Another Static Site Script
# By satoshi

import yaml
import os
import re
import jinja2
import markdown
import datetime
import codecs 
import termcolor
import bs4
import sys
from operator import itemgetter

runVerbose = False

for a in sys.argv:
  runVerbose = runVerbose or ( a == '--verbose' or a == '-v' )

UNPUBLISHED_TAG = '--unpublished--\n'
TEASER_TAG = '<!--break-->'
NOTINFRONT_TAG = '--not infront--\n'

# these header tags still not implemented
CONFIDENTIAL_TAG = '--condidential--\n'
HIGHLYCONFIDENTIAL_TAG = '--highly confidential--\n'

FIELD_STR = "field_"
# reading settings files
mainConfig = yaml.load( open( './config.yaml' ).read( ) )
themeConfig = yaml.load( open( './themes/%s/theme.yaml' % mainConfig['theme'] ) )
blockConfig = yaml.load( open( './block/block.yaml' ) )

# procesando nodos
try:
  os.mkdir( '_nodecache' )
except OSError:
  pass

if ( mainConfig['site']['useTeasers'] ):
  try:
    os.mkdir( './_nodecache/_teasercache' )
  except OSError:
    pass

print termcolor.colored( 'Welcome to YASSS - Your Awesome Static Site Script', 'green' , attrs=['bold'] )
print termcolor.colored( '(This release is still in a pre-alpha state. Use this under your own risk.)', 'yellow' )
print termcolor.colored( '-----------------------------------------------------------------------------------', 'yellow' )
print termcolor.colored( 'Reading your node files. We\'re going to be here for a hwhile... :)', 'red' )
taxonomy = dict( )
nodes = []

for nid, nodeFileName in enumerate( os.listdir( './node' ) ):
  if ( runVerbose ): print termcolor.colored( 'Processing for node %s.' % nodeFileName, 'blue' )
  if nodeFileName.endswith( '.txt' ):
    filePath = './node/%s' % nodeFileName
    nodeFileMeta = {}
    nodeFileMeta['taxonomy'] = dict()
    nodeFileMeta['author'] = mainConfig['author']['screenName']
    nodeFileMeta['unpublished'] = False
    nodeFileMeta['confidential'] = False
    nodeFileMeta['hconfidential'] = False
    nodeFileMeta['filename'] = nodeFileName
    nodeFileMeta['publishdate'] = datetime.datetime.fromtimestamp(  os.path.getctime('./node/' + nodeFileName ) )
    nodeTitle = nodeFileName.replace( '.txt', '' ).replace( '_', ' ' )
    nodeFileMeta['title'] = nodeTitle
    nodeFileMeta['infront'] = True
    nodeFileMeta['link'] = nodeFileName + '.html'
    nodeFileMeta['nid'] = nid

    # generando la url completa del nodo
    nodepath = mainConfig['site']['nodeUrlForm'].strip( '/' )

    #nodeFileMeta['fields'] = None
    i=0

    
    nodeFile = codecs.open( filePath, 'r', 'utf-8' ).readlines( )
    # leyendo un archivo (nodo) linea por linea
    for nodeFileLine in nodeFile:
      # reading header tags
      if ( re.match( '^\-\-[a-z|\ ]+\-\-$', nodeFileLine ) != None ):
        # print nodeFileLine
        if ( nodeFileLine == UNPUBLISHED_TAG ):
          nodeFileMeta['unpublished'] = True
        if ( nodeFileLine == CONFIDENTIAL_TAG ):
          nodeFileMeta['confidential'] = True
        if ( nodeFileLine == HIGHLYCONFIDENTIAL_TAG ):
          nodeFileMeta['hconfidential'] = True
        if ( nodeFileLine == NOTINFRONT_TAG ):
          nodeFileMeta['infront'] = False

        i = i + 1
      elif ( re.match( '^.+\:.*(\,)*$', nodeFileLine ) != None ):
        param = nodeFileLine.split( ':' )[0]
        values = [e.strip( ) for e in nodeFileLine.split( ':' )[1].split(',')]

        if ( param == 'author' ):
          nodeFileMeta['author'] = values[0]
        elif ( param == 'publishdate' ):
          dateVal = nodeFileLine[nodeFileLine.index( ':' )+1::].strip( ' \n' )
          try: 
            nodeFileMeta['publishdate'] = datetime.datetime.strptime( dateVal, mainConfig['site']['dateTimeFormat'] ) 
          except ValueError: 
            try:
              nodeFileMeta['publishdate'] = datetime.datetime.strptime( dateVal, mainConfig['site']['dateFormat'] )
            except ValueError:
              pass

        elif ( param == 'title' ):
          nodeTitle = nodeFileMeta['title'] = nodeFileLine.split( ':' ,1 )[1]
        elif( param == 'link' ):
          nodeFileMeta['link'] = values[0]
        elif( FIELD_STR in param ):
          ff = param[len( 'field_')::].split('_')
          nodeFileMeta['fields'] = {}
          fieldName = ff[1] 
          fieldKind = ff[0]
          nodeFileMeta['fields'][fieldName] = { u'val': values[0], u'kind': fieldKind }
          # print nodeFileMeta
        else:
          if ( param.strip( ' ' ) != '' ):
            nodeFileMeta['taxonomy'][param] = values
          try: 
            if ( taxonomy[param] != None ):
              pass
          except KeyError:
            if param.split(' ') != '':
              taxonomy[param] = {}

          for value in values:
            if nodeFileMeta['unpublished'] == True: continue
            if value.split(' ') != '':
              try:
                taxonomy[param][value]['count'] = taxonomy[param][value]['count'] + 1
                taxonomy[param][value]['nid'] = taxonomy[param][value]['nid'] + [ nid ]
              except KeyError:
                taxonomy[param][value] = {}
                taxonomy[param][value]['count'] = 1
                taxonomy[param][value]['nid'] = [ nid ]


        i = i + 1
      elif ( re.match( '^\-+$', nodeFileLine ) != None ):
        i = i + 1
        break
      else:
        break
    # /for termino de leer linea por linea del archivo
    
    # path for taxonomy terms
    taxonomyTerms = re.findall( '_T([\w]*)', nodepath )
    if ( taxonomyTerms != [] ):
      for vocabulary in taxonomyTerms:
        try:
          nodepath = nodepath.replace( '_T%s' % vocabulary, nodeFileMeta['taxonomy'][vocabulary][0] )
        except KeyError:
          nodepath = nodepath.replace( '_T%s' % vocabulary, '.' )
          
    if ( nodeFileMeta['link'][0] != '/' ): # la ruta sigue base/.../nodo
      nodepath = nodepath.replace( '_NDAY', "{:02d}".format( nodeFileMeta['publishdate'].day ) )
      nodepath = nodepath.replace( '_NMONTH', "{:02d}".format( nodeFileMeta['publishdate'].month ) )
      nodepath = nodepath.replace( '_NYEAR', "{:04d}".format( nodeFileMeta['publishdate'].year ) )

    else:
      nodepath = '.'
      os.system( 'mkdir -p ./_publish/%s' % nodeFileMeta['link'][0:nodeFileMeta['link'].rfind( '/' )] )


    nodeFileMeta['url'] = mainConfig['site']['baseUrl'].strip( '/' ) + '/' + nodepath.strip( '/' ) + '/' + nodeFileMeta['link'] + '.html'        
    nodeFileMeta['path'] = nodepath
      
    nodes = nodes + [ nodeFileMeta ]

    if ( runVerbose): print termcolor.colored( 'Writing temporary preprocessed file for "%s" (%s)' % ( nodeTitle, nodeFileName ), 'cyan' )

    # aplicar aqui el markdown para nodos
    t = markdown.markdown( ''.join( nodeFile[i::] ) ) 
    codecs.open( './_nodecache/%s.cache.txt' % nodeFileName.replace( '.txt','' ) , 'w', 'utf-8' ).write( t )

    if ( mainConfig['site']['useTeasers'] ):
      f = codecs.open( './_nodecache/%s.cache.txt' % nodeFileName.replace( '.txt','' ), 'r',  'utf-8' ).read( )
      if ( TEASER_TAG in f ):
        codecs.open( './_nodecache/_teasercache/%s.teaser.cache.txt' % nodeFileName.replace( '.txt','' ) , 'w', 'utf-8' ).write( f[0:f.index( TEASER_TAG )] )
      elif ( len( f ) < mainConfig['site']['teaserDefaultLimit'] ):
        codecs.open( './_nodecache/_teasercache/%s.teaser.cache.txt' % nodeFileName.replace( '.txt','' ) , 'w', 'utf-8' ).write( f )
      else:
        # revisar esto, si se trunca antes de cerrarse una marca html dara problemas
        # sugerencia rapida: usar beautifulsoup
#       codecs.open( './_nodecache/_teasercache/%s.teaser.cache.txt' % nodeFileName.replace( '.txt','' ) , 'w', 'utf-8' ).write( f[0:mainConfig['site']['teaserDefaultLimit']] )
        codecs.open( './_nodecache/_teasercache/%s.teaser.cache.txt' % nodeFileName.replace( '.txt','' ) , 'w', 'utf-8' ).write( f )

  # /if nodeFileName
# /for nodeFileName

# ordenando en funcion a la fecha
nodes_unsorted = nodes
nodes = sorted( nodes, key=itemgetter( 'publishdate' ), reverse=True )

print termcolor.colored( 'Reading menu files...', 'red' )
# procesando bloques autogenerados (menues, informacion de tags y similares)
# para los menues

menuTpl = jinja2.Template( codecs.open( './themes/%s/menu.tpl.html' % mainConfig['theme'], 'r', 'utf-8' ).read( ) )
templateMenues = {}

print 'Writing files for menu blocks...'
for menuFileName in os.listdir( './menu' ):
    print 'Processing for menu %s.' % menuFileName
    if menuFileName.endswith( '.menu.yaml' ):
      menuName = menuFileName.replace( 'menu.yaml', '' )
      altMenuTpl = None
      menuFile = yaml.load( open( './menu/%s' % menuFileName ).read( ) )

      for menuData in menuFile:
        if '|' in menuData:
          menuName = menuData.split( '|' )[1]
        menuStruct = menuFile[menuData]
        break

      if os.path.exists( './themes/%s/menu-%s.tpl.html' % ( mainConfig['theme'], menuName ) ):
        altMenuTpl = jinja2.template( codecs.open( './themes/%s/menu-%s.tpl.html' % ( mainConfig['theme'], menuName ), 'r', 'utf-8' ).read( ) )

      if menuName in themeConfig['thememenues']:
        # abrir el template de menu comun, generar la plantilla y agregar la plantilla a la variable template
        if not ( altMenuTpl is None ):
          templateMenues[ menuName ] =  altMenuTpl.render( menu = menuStruct )
        else:
          templateMenues[ menuName ] = menuTpl.render( menu = menuStruct )

      # aqui genero los bloques por cada menu
      if not ( altMenuTpl ) is None:
        thisMenu = altMenuTpl.render( menu = menuStruct )
      else: 
        thisMenu = menuTpl.render( menu = menuStruct )

      # aqui uso una suerte de mangling para los generar los bloques de los menues
      codecs.open( './block/_menu-%s.txt' % menuName, 'w', 'utf-8' ).write( thisMenu )
# para los bloques de taxonomias (solo bloques)
print 'Writing files for taxonomy vocabularies and terms blocks'
for vocabulary, terms in taxonomy.iteritems( ):
  if vocabulary.strip( ' ' ) == '':
    continue

  if ( os.path.exists( './themes/%s/yasss-taxonomy-T%s-block.tpl.txt' % ( mainConfig['theme'], vocabulary  ) ) ):
    yasssBlockTpl = jinja2.Template( codecs.open( './themes/%s/yasss-taxonomy-T%s-block.tpl.txt' % ( mainConfig['theme'], vocabulary  ), 'r',  'utf-8' ).read( ) )
    codecs.open( './block/_yasss-vocabulary-%s.txt' % vocabulary, 'w', 'utf-8' ).write( yasssBlockTpl.render( vocabulary = vocabulary, terms = terms ) )
  else:
    yasssBlockTpl = jinja2.Template( codecs.open( './themes/%s/yasss-taxonomy-block.tpl.txt' % ( mainConfig['theme'] ), 'r',  'utf-8' ).read( ) )
    codecs.open( './block/_yasss-vocabulary-%s.txt' % vocabulary, 'w', 'utf-8' ).write( yasssBlockTpl.render( terms = terms, vocabulary=vocabulary ) )
    
print 'Writing files for recent published nodes block...'
c = 0
recentNodes = []
for recentNode in nodes:
  c = c + 1
  if recentNode[ 'unpublished' ] or not recentNode[ 'infront' ]:
    continue
  recentNodes = recentNodes + [ recentNode ]
  if ( c > int( mainConfig['site']['recentPublishedLimit'] ) ):
    break

yasssBlockTpl = jinja2.Template( codecs.open( './themes/%s/yasss-recent-block.tpl.txt' % mainConfig['theme'], 'r', 'utf-8' ).read( ) )
codecs.open( './block/_yasss-recent.txt', 'w', 'utf-8' ).write( yasssBlockTpl.render( recentnodes = recentNodes  ) )

# procesando regiones
print 'Processing regions and blocks...'
regions = {}
# colocar aqui el procesamiento markdown para bloques
# a partir de ahora se tiene en consideracion la documentacion de https://www.drupal.org/node/104319
blockTemplate = jinja2.Template( codecs.open( './themes/%s/block.tpl.html' % mainConfig['theme'], 'r',  'utf-8' ).read( ) )

# procesa script
scriptHTMLTag = '<script language="javascript" src="%s/files/theme/%s/%s"></script>\n'
scripts = ''
for script in themeConfig['scripts']:
    scripts = scripts + scriptHTMLTag % ( mainConfig['site']['baseUrl'], mainConfig['theme'], script )

# procesa estilos
styleHTMLTag = '<link rel="stylesheet" href="%s/files/theme/%s/%s" type="text/css"></link>'
styles = ''
for style in themeConfig['stylesheets']:
  styles = styles + styleHTMLTag % ( mainConfig['site']['baseUrl'].strip( '/' ), mainConfig['theme'], style )



template = { 'stylesheets': styles ,'scripts': scripts, 'bottomscripts': '', 'menues': templateMenues, 'pagetitle': mainConfig['site']['siteName'] + '{% if node.title != \'\' %} | {{ node.title }} {% endif %}'  } 
print templateMenues

for region in themeConfig['regions']:
  regions[ region ] = ''
  usesRegionTemplate = os.path.exists( './themes/%s/block-%s.tpl.html' % ( mainConfig['theme'], region ) )

  if ( usesRegionTemplate ):
    blockRegionTemplate = blockTemplate = jinja2.Template( codecs.open( './themes/%s/block-%s.tpl.html' % ( mainConfig['theme'], region ), 'r',  'utf-8' ).read( ) )

  try:
    for blockFile in blockConfig[ region ]:
      if ( runVerbose ): print termcolor.colored( 'Reading block file for %s.' % blockFile, 'blue' )
      if ( '|' in blockFile ):
        bs = blockFile.split( '|' )
        blockSubject = bs[1].strip( )
        blockFileName = bs[0] + '.txt'

      else:
        blockSubject = blockFile.replace('_', ' ')
        blockFileName = blockFile + '.txt'

      blockContent = codecs.open( './block/%s' %  blockFileName, 'r', 'utf-8' ).read( )
      if ( usesRegionTemplate ):
        regions[ region ] = regions[ region ] + blockRegionTemplate.render( block={ 'subject': blockSubject, 'content': blockContent } )
      else:
        regions[ region ] = regions[ region ] + blockTemplate.render( block={ 'subject': blockSubject, 'content': blockContent } )
  except KeyError:
    pass


# generando plantilla 
# os.system( 'cp ./themes/%s/css ./_publish/ -R' % mainConfig['theme'] )

pageTemplate = jinja2.Template( codecs.open( './themes/%s/page.tpl.html' % mainConfig['theme'], 'r', 'utf-8' ).read( ) )

regions['content'] = '{{ content }}'

codecs.open( 'page.cache.tpl.html', 'w', 'utf-8' ).write( pageTemplate.render( regions = regions, template=template ) )

# generacion de paginas

# genero la pagina principal, en drupal suele ser base/node, conteniendo los 
# teasers, asi que eso es lo que haremos, ya que logicamente no estoy 
# implementando algo parecido a "vistas"

nodeTemplate = jinja2.Template( codecs.open( './themes/%s/node.tpl.html' % mainConfig['theme'], 'r', 'utf-8' ).read( ) )
allContents = ''

pageTemplate = jinja2.Template( codecs.open( './page.cache.tpl.html', 'r', 'utf-8' ).read( ) )

try:
  os.mkdir( '_publish' ) # debera commitear este directorio 

except OSError:
  pass

os.system( 'mkdir -p ./_publish/files/theme/%s/css' % mainConfig['theme'] )
os.system( 'mkdir -p ./_publish/files/theme/%s/js' % mainConfig['theme'] )

os.system( 'cp ./themes/%s/css/* ./_publish/files/theme/%s/css -R' % ( mainConfig['theme'], mainConfig['theme'] ) )
os.system( 'cp ./themes/%s/js/* ./_publish/files/theme/%s/js -R' % ( mainConfig['theme'], mainConfig['theme'] ) )



commentTpl = jinja2.Template( codecs.open( './themes/%s/comment.tpl.html' % mainConfig['theme'], 'r', 'utf-8' ).read( ) )

print 'Writing node pages...'
for node in nodes:
  if ( node['unpublished'] ):
    continue

  
  nodefile = node['filename'].replace( '.txt','.teaser.cache.txt' )
  n = node
  
  # n['url'] = mainConfig['site']['baseUrl'].strip( '/' ) + '/' + nodepath.strip( '/' ) + '/' + node['link'] + '.html'

  # print n['infront']
  n['content'] = bs4.BeautifulSoup( codecs.open( './_nodecache/_teasercache/%s' % nodefile, 'r', 'utf-8' ).read( ) ).prettify( )
  if ( n['infront'] ):
    nodeText = nodeTemplate.render( node = n )
    allContents = allContents + nodeText 
  
  if not( node['link'][-5::] == '.html' or node['link'][-4::] == '.htm' ):
    n['link'] = n['link'] + '.html'

  # ahora procesa las paginas
  n['content'] = codecs.open( './_nodecache/%s' % node['filename'].replace( '.txt', '.cache.txt' ), 'r', 'utf-8' ).read( ) + commentTpl.render( node = n )

  # observar aqui lo relacionado al formato de un link. Por ejemplo /anho/mes/dia/nodo
  os.system( 'mkdir -p ./_publish/%s' % n['path'] )
  pageRender = pageTemplate.render( node=n, content = nodeTemplate.render( node = n, template = { 'scripts' : scripts, 'stylesheets': styles } )  ) 
  codecs.open( './_publish/%s/%s' % ( n['path'], node['link'] ), 'w', 'utf-8' ).write( pageRender )   

np = {}
np['title'] = ''

codecs.open( './_publish/node.html', 'w', 'utf-8' ).write( pageTemplate.render( regions = regions, content  = allContents, template = template, node=np ) )

# generacion de paginas de terminos de taxonomias
print 'Writing taxonomy vocabularies and terms pages.'
for vocabulary, terms in taxonomy.iteritems( ):
  for term, data in terms.iteritems():
    if term.strip(' ') == '':
      continue

    allContents = ''
    listTermNodes = []
    for termNodeId in data['nid']:
      if ( n['unpublished'] ):
        continue
      listTermNodes = listTermNodes + [ nodes_unsorted[ termNodeId ] ]

    # ordenar
    listTermNodes = sorted( listTermNodes, key=itemgetter( 'publishdate' ), reverse=True )


    for n in listTermNodes:
      nodefile = n['filename'].replace( '.txt','.teaser.cache.txt' )

      n['content'] = bs4.BeautifulSoup( codecs.open( './_nodecache/_teasercache/%s' % nodefile, 'r', 'utf-8' ).read( ) ).prettify( )

      nodeText = nodeTemplate.render( node = n )
      allContents = allContents + nodeText
    # fin procesar los nodos de termino de taxonomia
    # generando la pagina para el termino
    pageRender = pageTemplate.render( node={ 'title' : '%s : %s' % ( vocabulary, term ) }, content = allContents )
    os.system( 'mkdir -p ./_publish/%s' % vocabulary )
    codecs.open( './_publish/%s/%s.html' % ( vocabulary, term ), 'w', 'utf-8' ).write( pageRender )
    
print termcolor.colored( 'Done.', 'red' )

